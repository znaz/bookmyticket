import React from 'react';
import ReactDOM from 'react-dom/client';
import './index.css';
import {
  createBrowserRouter,
  RouterProvider,
} from "react-router-dom";
import RootLayout from './Routes/RootLayout';
import ErrorPage from './ErrorPage';
import MoviesPage, {loader as moviesLoader} from './Routes/MoviesPage';
import Homepage, {loader as homeLoader} from './Routes/Homepage';
import SingleMoviePage, {loader as singleMovieLoader} from './Routes/SingleMoviePage';
import PersonPage, {loader as personLoader} from './Routes/PersonPage';
import BookingPage, {loader as bookingLoader} from './Routes/BookingPage';
import SelectSeat, {loader as selectSeatLoader} from './Routes/SelectSeat';
import store from './Store/store';
import { Provider } from 'react-redux';
import Paymentpage from './Routes/Paymentpage';
import RecieptPage from './Routes/RecieptPage';
import SignInPage from './Routes/SignInPage';
import SignUpPage from './Routes/SignUpPage';



const router = createBrowserRouter([
  {
    path: "/",
    element: <RootLayout/>,
    errorElement: <ErrorPage/>,
    children: [
      {
        path: "/",
        element: <Homepage/>,
        loader: homeLoader
      },
      {
        path: "/movies",
        element:<MoviesPage/>,
        loader : moviesLoader
      },
      {
        path:"/movies/:movieId",
        element: <SingleMoviePage/>,
        loader : singleMovieLoader
      },
      {
        path: "/person/:personId",
        element:<PersonPage/>,
        loader: personLoader
      },
      {
        path : "/booking-page/:movieId",
        element: <BookingPage/>,
        loader : bookingLoader
      },
      {
        path : "/select-seats/:showId",
        element : <SelectSeat/>,
        loader : selectSeatLoader
      },
      {
        path : "/checkout",
        element : <Paymentpage/>
      },
      {
        path : '/reciept',
        element : <RecieptPage/>
      },
      {
        path: '/signIn',
        element : <SignInPage/>
      },
      {
        path: '/signup',
        element : <SignUpPage/>
      }


    ]
  },
]);

const root = ReactDOM.createRoot(document.getElementById('root'));
root.render(
  <React.StrictMode>
    <Provider store={store}>
      <RouterProvider router={router} />
    </Provider>  
  </React.StrictMode>
);
