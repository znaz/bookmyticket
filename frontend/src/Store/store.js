import { configureStore } from '@reduxjs/toolkit'
import screenReducer from './screenSlice'
import authReducer from './authSlice'

export default configureStore({
  reducer: {
    screen : screenReducer,
    auth : authReducer
  }
})