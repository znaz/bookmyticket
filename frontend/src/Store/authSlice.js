import { createSlice } from "@reduxjs/toolkit";

const saveUser = (user) => {
    localStorage.setItem("user", JSON.stringify(user));
  };
  
const data = () => {
    const user = localStorage.getItem("user");
    return user ? JSON.parse(user) : null;
  };

export const authSlice = createSlice({
  name: "auth",
  initialState: {
    user : data()
},
reducers : {
    addUser : (state, action)=>{
        state.user = action.payload

        saveUser(action.payload)
    },
    removeUser : (state)=>{
      state.user = null
    }
}
});

export const{addUser, removeUser} = authSlice.actions

export default authSlice.reducer