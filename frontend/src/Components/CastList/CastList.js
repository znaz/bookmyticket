import React from "react";
import { Link } from "react-router-dom";
import styles from "./CasList.module.css"

function CastList(props) {
  const cast = props.cast;
  return (
    <Link className={styles.castLink} to={"/person/"+cast.person._id}>
    <li className={styles.Cast}>
      <img src={cast.person.image} alt={cast.person.name} />
      <h4>{cast.person.name}</h4>
      <span>{cast.role}</span>
    </li>
    </Link>
    
  );
}

export default CastList;
