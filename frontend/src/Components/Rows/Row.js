import React from "react";
import Seats from "../Seats/Seats";
import styles from './Row.module.css'

function Row(props) {
    const row = props.row
    const seatArray = new Array(row.numberOfSeats).fill("")
  return (
    <li className={styles.row}>
      <span>{row.name}</span>
      <ul className={styles.seatList}>
        {seatArray.map((seat, index) => {
          return <Seats key={index} seat={seat} index={index} rowName={props.row.name} price={props.price}/>;
        })}
      </ul>
    </li>
  );
}

export default Row;
