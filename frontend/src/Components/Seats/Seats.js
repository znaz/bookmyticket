import React, { useEffect, useState } from "react";
import styles from "./Seats.module.css";
import { selectSeat, deselectSeat } from "../../Store/screenSlice";
import { useDispatch, useSelector } from "react-redux";

function Seats(props) {
  const [status, setStatus] = useState("available");
  const bookedSeats = useSelector(state=>state.screen.bookedSeats)

  const seatNumber = props.index + 1;
  const rowName = props.rowName
  const dispatch = useDispatch();
  
  useEffect(()=>{
    const result = bookedSeats.filter(seat=> seat.seatNumber === seatNumber && seat.rowName === rowName)
  
    if(result.length !== 0){
      setStatus("booked")
    }

  },[bookedSeats])
  const handleSelect = () => {
    if (status === "available") {
      setStatus("selected");
      dispatch(
        selectSeat({ seatNumber, rowName, price: props.price })
      );
    } else if(status==="selected") {
      setStatus("available");
      dispatch(
        deselectSeat({ seatNumber, rowName, price: props.price })
      );
    }
    else{
      
    }
  };
  return (
    <li
      className={
        styles.seats + " " + (status === "selected" ? styles.Selected : "")+ " "+ (status === "booked"? styles.Booked : "")
      }
      onClick={() => {
        handleSelect();
      }}
    >
      <span>{seatNumber}</span>
    </li>
  );
}

export default Seats;
