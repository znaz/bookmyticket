import React from "react";
import Row from "../Rows/Row";
import styles from './Tier.module.css'

function Tier(props) {
    const tier = props.tier
    const price = tier.price
  return (
    <li className={styles.Tier}>
      <h4>
        {tier.name}-Rs {tier.price}
      </h4>
      
      <ul className={styles.RowList}>
        {tier.rows.map((row, index) => {
          return <Row key={row._id} row={row} price={price} />;
        })}
      </ul>
    </li>
  );
}

export default Tier;
