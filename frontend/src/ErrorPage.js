import React from 'react'
import styles from './ErrorPage.module.css'

function ErrorPage() {
  return (
    <>
    <div className={styles.ErrorPage}>
        <h2>Sorry Error 404!</h2>
        <span>Not Found</span>
    </div>
    </>
  )
}

export default ErrorPage