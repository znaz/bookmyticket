import { useSelector } from "react-redux";
import {  useNavigate } from "react-router-dom";
import styles from "./Payment.module.css";
import axios from "axios";
import { url } from "../url";




function Paymentpage() {
  const navigate = useNavigate();
  const seats = useSelector((state) => state.screen.selectedSeats)
  const selectedSeats = seats.map((seat)=>{
    return {seatNumber:seat.seatNumber,rowName:seat.rowName}
  })
  
  const totalPrice = useSelector((state) => state.screen.totalPrice);
  const selectedShow = useSelector((state)=> state.screen.selectedShow)
  console.log(seats)
  const handleSubmit = (e)=>{
    
    e.preventDefault()
    const form = e.target

    const email = form['email'].value
    const phone = form['phone'].value

    const bookingPayload = {
      email,
      phone,
      selectedSeats,
      selectedShow
    }
  axios.post(url+'/bookings',bookingPayload)

      .then(res=>{
        console.log(res)
        navigate('/reciept')
      })
      .catch(err=>{
        console.log(err)
      })
      
  }
  
  return (
    <main className={styles.main}>
      <div className={styles.BookingSummary}>
        <h2>Booking Summary</h2>

        <ul className={styles.seatList}>
          {seats.map((seat, index) => {
            return (
              <li key={index}>
                <div className={styles.selectedSeat}>
                  <h3>
                    {seat.rowName}
                    {seat.seatNumber}
                  </h3>
                  <h3>{seat.price} Rs</h3>
                </div>
              </li>
            );
          })}
        </ul>
        <form id={styles.details} onSubmit={handleSubmit}>
          <label htmlFor="email">Email</label>
          <input type="email" name="email" />
          <label htmlFor="phone">Phone</label>
          <input type="string" name="phone" />
          <button className={styles.proceedButton }>
            <span id={styles.totalPrice}>TOTAL: Rs. {totalPrice}</span>
            <span id={styles.Proceed}>Proceed</span>
          </button>
        </form>
      </div>
    </main>
  );
}

export default Paymentpage;
