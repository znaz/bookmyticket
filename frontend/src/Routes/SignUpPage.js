import React from 'react'
import styles from './SignUpPage.module.css'
import { Link } from 'react-router-dom'
import { object, string  } from 'yup'
import axios from 'axios'
import { url } from '../url'

function SignUpPage() {

  const  userSchema = object({
    name: string().required(),
    email: string().required().email(),
    password: string().required().min(6, 'must be at least 6 characters long')

  })

  const handleSubmit = async(e)=>{
    e.preventDefault()

    const form = e.target

    const name = form['name'].value
    const email = form['email'].value
    const password = form['password'].value
    const confirmPassword = form['confirmPassword'].value


    let valid= false

    try{
      const user = await userSchema.validate({
        name: name,
        email:email,
        password:password
      })
      

      if(password === confirmPassword){
        let valid = true
        if(valid){
      console.log("password is valid")
      axios.post(url+'/users/signup', {
        name,
        email,
        password
      })
      .then(res=>{
        console.log(res)
      })
      .catch(err=>{
        console.log(err)
      })
    }
      }
    }
    catch(err){
      console.log(err)
    }

  }
  return (
    <main className={styles.signupMain}>
        <form className={styles.signupForm} onSubmit={handleSubmit}>

        <h2>Sign Up</h2>

          <div>
            <label htmlFor='name'>Name</label>
            <input type='text' id='name' placeholder='Enter your Name'/>
          </div>
          <div>
            <label htmlFor='email'>Email</label>
            <input type='email' id='email' placeholder='Enter your Phone email'/>
          </div>
          <div>
            <label htmlFor='password'>Password</label>
            <input type='password' id='password' placeholder='Enter your password'/>
          </div>
          <div>
            <label htmlFor='password'>Confirm Password</label>
            <input type='password' id='confirmPassword' placeholder='Re-Enter your password'/>
          </div>
          <button className={styles.SignupButton} type='submit'>
            SIGN UP
          </button>
          <Link to={'/signIn'} className={styles.signupLink}>Already have an account?/SignIn</Link>
        </form>
    </main>
  )
}

export default SignUpPage