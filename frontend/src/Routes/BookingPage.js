import React from "react";
import { useLoaderData } from "react-router-dom";
import axios from "axios";
import styles from "./Bookingpage.module.css";
import dayjs from "dayjs";
import { Link } from "react-router-dom";
import { url } from "../url";

export async function loader({ params }) {
  const res = await axios.get(
    url+"/movies/" + params.movieId + "/shows"
  );
  const shows = res.data;
  return shows;
}

function BookingPage() {
  const shows = useLoaderData();
  return (
    <main>
      <div className={styles.container}>
        <h2>Shows Available</h2>

        <ul className={styles.ShowList}>
          {shows.map((show, index) => {
            return (
              <Link key={index} className={styles.showLink} to={'/select-seats/'+show._id}>
                <li className={styles.shows} key={index}>
                  <span>Screen {show.screen.screenNumber}</span>
                  <span>{dayjs(show.showTime).format("DD MMM HH:mm")}</span>
                </li>
              </Link>
            );
          })}
        </ul>
      </div>
    </main>
  );
}

export default BookingPage;
