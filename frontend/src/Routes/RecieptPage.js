import React from "react";
import styles from "./RecieptPage.module.css";
import tickSymbol from "../Images/Untitled design (2).png"
import { Link } from "react-router-dom";

function RecieptPage() {
  return (
    <main className={styles.RecieptMain}>
      <div className={styles.Reciept}>
        <div className={styles.successful}>
        <h3>Booking Successful </h3>
        <img src={tickSymbol} alt="tick" />
        </div>
        <Link to="/" className={styles.goback}>Back to homepage</Link>
      </div>
      
    </main>
  );
}

export default RecieptPage;
