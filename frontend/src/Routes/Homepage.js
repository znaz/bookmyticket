import React from 'react'
import axios from 'axios'
import styles from './Homepage.module.css'
import { useLoaderData } from 'react-router-dom'
import Moviecard from '../Components/MovieCard/Moviecard'
import { url } from '../url'

export async function loader(){
  const res = await axios.get(url+'/movies')
   const movies = res.data
   return{movies}
  
}
function Homepage() {
 
  const {movies} = useLoaderData()
  return (
    
    <>
    <main>
        <section className={styles.SectionMovies} >
              <div className={styles.container}  >
                <h2>Latest Releases</h2>
              <ul className={styles.movieList}>
                {
                  movies.filter((movie,index)=> index < 5).map((movie,index)=>{
                    return <Moviecard key={index} movie={movie}/>
                  })
                }
              </ul>
              </div>
        </section>
    </main>
    </>
  )
}

export default Homepage