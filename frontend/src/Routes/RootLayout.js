import React from "react";
import { Outlet,Link, useNavigate } from "react-router-dom";
import styles from "./RootLayout.module.css";
import SignIn from "../Images/icons8-person-24.png"
import { useDispatch, useSelector } from "react-redux";
import axios from "axios";
import { removeUser } from "../Store/authSlice";
import { url } from "../url";

function RootLayout() {
  const user = useSelector(state=> state.auth.user)
  const dispatch = useDispatch()
  const navigate = useNavigate()
  const handleLogout = ()=>{
    axios.post(url+"/users/logout",{},{withCredentials:true})
    .then(res=>{
      console.log(res)
      dispatch(removeUser())
      navigate("/signin")
    })
    .catch(err=> console.log(err))
  }
  return (
    <>
      <header className={styles.header}>
        <Link className={styles.logo} to={'/'}>
          BookMyTicket
        </Link>
        <nav className={styles.headerNav}> 
          <ul className={styles.navlist}>
            <li>
            <Link to={'/'}>Home</Link>
            </li>
            <li>
            <Link to={'/movies'}>Movies</Link>
            </li>
            <li>
            <Link to={''}>Events</Link>
            </li>
            {user&&<li>
            <Link onClick={handleLogout} to={'/logout'}>Logout</Link>
            </li>}
            
          </ul>
          {!user?<div className={styles.SignIn}>
              <img id={styles.SignInImage} src={SignIn} alt="signin"/>
            <Link className={styles.signInText} to={'/SignIn'}>Sign In</Link>
          </div> : <div className={styles.profile}>
              
              <span className={styles.profileChar}>{user.name.charAt(0)}</span>
            </div>}
        </nav>
      </header>
      <Outlet/>
      <footer className={styles.footer}>
        <span>&copy; BookMyTicket
        </span>
        <span>Devoloped by Znaz</span>
      </footer>
    </>
  );
}

export default RootLayout;
