import React, { useEffect } from 'react'
import axios from 'axios'
import { useLoaderData,useNavigate} from 'react-router-dom';
import Tier from '../Components/Tier/Tier';
import styles from './SelectSeat.module.css'
import { useSelector,useDispatch } from 'react-redux';
import {addTiers,selectShow,setBookedseats} from '../Store/screenSlice'
import { url } from '../url';

export async function loader({ params }) {
    const res = await axios.get(
      url+"/shows/" + params.showId
    );
    const show = res.data;
    const bookingRes = await axios.get(url+'/bookings?show='+params.showId)
    const bookingList = bookingRes.data

    const bookedseats = []

    bookingList.map((booking)=>{
      return booking.selectedSeats.map((seat)=>{
      return bookedseats.push(seat)
      })
    })

console.log(bookedseats)
    
    return {show,bookedseats};


    
  }
function SelectSeat() {
    const dispatch = useDispatch()
    const navigate = useNavigate();
    const {show,bookedseats} = useLoaderData()
    const screen = show.screen
    const showId = show._id
    const screenData = useSelector(state => state.screen)
    const totalprice = useSelector(state=> state.screen.totalPrice)
    
    useEffect(()=>{
      dispatch(addTiers(screen.tiers))
      dispatch(setBookedseats(bookedseats))
    },[])

    const handleClick = ()=>
    {
      if(totalprice !== 0){
        dispatch(selectShow(showId))
      navigate('/checkout')
      }
      else{

      }

    }
  return (
    <main className={styles.SelectSeatmain}>     
        <ul className={styles.SeatUl}>
            {
                screenData.tiers.map((tier, index)=>{
                    return <Tier key={tier._id} tier = {tier}/>
                })
            }
        </ul>
            <div className={styles.BigScreen}>&nbsp;</div>
            <button id={styles.PayButton} onClick={()=>handleClick()}>Pay Rs:{totalprice}</button>
    </main>
  )
}

export default SelectSeat