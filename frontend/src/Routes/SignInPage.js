import React from 'react'
import styles from './SignInPage.module.css'
import { Link, useNavigate } from 'react-router-dom'
import axios from 'axios'
import { object, string  } from 'yup'
import { useDispatch } from 'react-redux'
import { addUser } from '../Store/authSlice'
import { url } from '../url'


function SignInPage() {

  const dispatch = useDispatch()
  const navigate = useNavigate()
  const userSchema = object({
    email : string().required(),
    password : string().required().min(6,"must be 6 characters")
  })

  const handleSubmit = async(e)=>{
    e.preventDefault()

    const form = e.target

    const email = form['email'].value
    const password = form['password'].value
    try{
      const user = await userSchema.validate({
        email : email,
        password : password
      })

        axios.post(url+'/users/signin',{
          email,
          password
        })
        .then(res=>{
          const user = res.data.user
          if(user){
            dispatch(addUser(user))
            navigate('/movies')
          }
        })
        .catch(err=>{
          console.log(err)
        })
      }
    
    catch(err){
      console.log(err)
    }
    
  }
  return (
    <main className={styles.signinMain}>
        <form className={styles.signinForm} onSubmit={handleSubmit}>

          <h2>Sign In</h2>
          <div>
            <label>Email</label>
            <input type='text' id='email' placeholder='Enter your email'/>
          </div>
          <div>
            <label>Password</label>
            <input type='password' id='password' placeholder='Enter your password'/>
          </div>
          <button className={styles.SigninButton} type='submit'>
            SIGN IN
          </button>
          <Link to={'/signup'} className={styles.signupLink}>Don't have an account?/SignUp</Link>
        </form>
    </main>
  )
}

export default SignInPage