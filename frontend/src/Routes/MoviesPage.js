import React from 'react'
import axios from 'axios'
import { useLoaderData } from 'react-router-dom'
import Moviecard from '../Components/MovieCard/Moviecard'
import styles from './Movies.module.css'
import { url } from '../url'

export async function loader(){
  const res = await axios.get(url+'/movies')
   const movies = res.data
   return{movies}
  
}
function MoviesPage() {

  const {movies} = useLoaderData()
  return (
    <>
    <main>
      <section className={styles.SectionMovies}>
        <div  className={styles.container}>
        <h2>AL Movies</h2>
          <ul className={styles.MoviesLists}>
            {
              movies.map((movie,index)=>{
                return <Moviecard key={index} movie={movie}/>
              })
            }
          </ul>

        </div>
      </section>
        
    </main>
    </>
  )
}

export default MoviesPage