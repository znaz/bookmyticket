import React from "react";
import { Link } from "react-router-dom";
import { useLoaderData } from "react-router-dom";
import axios from "axios";
import styles from "./SingleMovie.module.css";
import CastList from "../Components/CastList/CastList";
import { url } from "../url";

export async function loader({ params }) {
  const res = await axios.get(url+"/movies/" + params.movieId);
  const movie = res.data;

  const castRes = await axios.get(url+"/movies/" + params.movieId+ "/cast")
  const cast = castRes.data
  return { movie, cast };
}

function SingleMoviePage() {
  const { movie, cast } = useLoaderData();
  return (
    <main>
      <section className={styles.SectionMovie}>
        <div className={styles.container}>
          <img
            className={styles.movieImage}
            src={movie.image}
            alt={movie.title + "thumbnail"}
          />
          <div className={styles.movieDetails}>
            <h3>{movie.title}</h3>
            <span className={styles.lang}>{movie.language}</span>
            <span>{movie.category}</span>
            <Link className={styles.BookButton} to={"/booking-page/"+movie._id}>
              Book Now
            </Link>
          </div>
        </div>
      </section>
      <section className={styles.SectionAbout}>
        <div className={styles.container}>
              <h3>About</h3>
              <p>{movie.description}</p>
        </div>
      </section>
      <section className={styles.SectionCast}>
      <div className={styles.container}>
              <h3>Cast</h3>
              <ul className={styles.castList}>
                  {
                    cast.map((cast,index)=>{
                      return <CastList key={index} cast={cast}/>
                    })
                  }
              </ul>
        </div>
      </section>
    </main>
  );
}

export default SingleMoviePage;
