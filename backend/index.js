const express = require("express");
const mongoose = require("mongoose");
require('dotenv').config()



const movieRoutes = require("./routes/moviesRoutes");
const personRoutes = require("./routes/personsRoutes");
const languageRoutes = require("./routes/languageRoutes");
const castRoutes = require("./routes/castRoutes")
const theatreRoutes = require("./routes/theatreRoutes")
const showRoutes = require("./routes/showRoutes")
const screenRoutes = require("./routes/screenRoutes")
const bookingRoutes = require("./routes/bookingRoutes")
const userRoutes = require("./routes/userRoutes")

const cors = require("cors");
const app = express();
const port = 3000;

app.use(
  cors({
    origin: ["http://localhost:3001","https://65c49ae8eb7081505354876f--celebrated-heliotrope-e34fae.netlify.app"],
    methods: ["GET", "POST", "PATCH", "DELETE"],
    credentials: true,
  })
);
app.use(express.json());

app.use("/movies", movieRoutes);
app.use("/persons", personRoutes);
app.use("/languages", languageRoutes);
app.use("/casts", castRoutes)
app.use("/theatres", theatreRoutes)
app.use("/shows", showRoutes)
app.use("/screens", screenRoutes)
app.use("/bookings",bookingRoutes)
app.use("/users", userRoutes)

main()
  .then((data) => console.log("db connected"))
  .catch((err) => console.log(err));

app.listen(port, () => {
  console.log(`Example app listening on port ${port}`);
});

async function main() {

  const dbUrl = process.env.DB_URL

  const urlWithPassword = dbUrl.replace("<password>",process.env.DB_PASSWORD)

  await mongoose.connect(urlWithPassword);
}
