const express = require("express");
const router = express.Router();
const Movies = require("../models/movie");
const Cast = require("../models/cast")
const Person = require("../models/person")
const Show = require("../models/show")
const Theatre = require('../models/theatre')

router.post("/", async (req, res, next) => {
  const movie = new Movies(req.body);
  await movie.save();
  res.status(201).json(movie);
});

router.get("/", async (req, res, next) => {
  const movies = await Movies.find({});
  res.status(200).json(movies);
});

router.get("/:movieId", async (req, res, next) => {
    const movie = await Movies.findById(req.params.movieId)
    res.status(200).json(movie);
  
});

router.get("/:movieId/cast", async (req, res, next) => {
  const casts = await Cast.find({movie : req.params.movieId}).populate('person')
  res.status(200).json(casts);

});

router.get("/:movieId/shows", async (req, res, next) => {
  const shows = await Show.find({movie : req.params.movieId}).populate('screen').populate('movie')
  res.status(200).json(shows);

});


module.exports = router;
