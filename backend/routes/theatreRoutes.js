const express = require("express");
const router = express.Router();
const Theatre = require("../models/theatre");

router.post("/", async (req, res, next) => {
  const theatre = new Theatre(req.body);
  await theatre.save();
  res.status(201).json(theatre);
});

router.get("/", async (req, res, next) => {
  const theatre = await Theatre.find({});
  res.status(200).json(theatre);
});

module.exports = router;