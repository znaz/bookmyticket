const express = require("express");
const jwt = require('jsonwebtoken');
const User = require("../models/users");
const bcrypt = require('bcrypt');
const router = express.Router();
const saltRounds = 10;



router.post("/verify", async (req, res, next) => {
    try{
        const token = req.cookies.token
        if(!token){
            return res.json({message : "user not logged in"})
        }
        return res.send("logged in")
    }
    catch(error){
        res.status(500).send("Error Occured")
    }
});

router.post("/signup", async (req, res, next) => {
    const hash = bcrypt.hashSync(req.body.password, saltRounds);
    const user = new User({...req.body,password : hash});
    await user.save();
    res.status(201).json(user);
});

router.post("/signin", async (req, res, next) => {
    try{
        const {email,password} = req.body
        console.log(email)

        const user = await User.findOne({email})

        
        if(!user){
            return res.json({message :'Incorrect password or email'}) 
        }
        const auth = bcrypt.compareSync(password, user.password);
        
        if(!auth){
            return res.json({message :'Incorrect password or email'})
             
        }
        const token = jwt.sign({ name: user.name, id : user._id }, process.env.JWT_TOKEN)
        
        res.cookie("token",token,{
            withCredentials: true,
            httpOnly: false,
        })
        res.status(201).json({user:{name : user.name, id : user._id}});
    }
    catch(err){
        res.status(500).send("Error Occured")
}
});

router.post("/logout", async (req, res, next) => {
    try{
        res.cookie('token', "", {expires: new Date(0)})
        res.status(204).send("Logout successfully")
    }
    catch(err){
        res.status(500).send("Error Occured")
    }
});
module.exports = router;