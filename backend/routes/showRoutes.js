const express = require("express");
const router = express.Router();
const Show = require("../models/show");

router.post("/", async (req, res, next) => {
  const show = new Show(req.body);
  await show.save();
  res.status(201).json(show);
});

router.get("/", async (req, res, next) => {
  const show = await Show.find({});
  res.status(200).json(show);
});

router.delete('/:showId', async (req, res, next) => {
  const deletedShow = await Show.findByIdAndDelete(req.params.showId);
  res.status(200).json(deletedShow)
})

router.get("/:showId", async (req, res, next) => {
  const show = await Show.findById(req.params.showId).populate('screen')
  res.status(200).json(show);
});

module.exports = router;