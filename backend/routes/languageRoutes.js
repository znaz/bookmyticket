const express = require("express");
const router = express.Router();
const Languages = require("../models/language");

router.get("/", async (req, res, next) => {
  const languages = await Languages.find({});
  res.status(200).json(languages);
});

module.exports = router;
