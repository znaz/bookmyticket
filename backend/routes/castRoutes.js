const express = require("express");
const router = express.Router();
const Cast = require("../models/cast");

router.post("/", async (req, res, next) => {
  const cast = new Cast(req.body);
  await cast.save();
  res.status(201).json(cast);
});

router.get("/", async (req, res, next) => {
  const cast = await Cast.find({});
  res.status(200).json(cast);
});

module.exports = router;