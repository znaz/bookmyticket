const express = require("express");
const router = express.Router();
const Screen = require("../models/screen");

router.post("/", async (req, res, next) => {
  const screen = new Screen(req.body);
  await screen.save();
  res.status(201).json(screen);
});

router.get("/", async (req, res, next) => {
  const screen = await Screen.find({});
  res.status(200).json(screen);
});

module.exports = router;