const express = require("express");
const router = express.Router();
const Persons = require("../models/person")

router.post("/", async (req, res, next) => {
  const person = new Persons(req.body);
  await person.save();
  res.status(201).json(person);
});

router.get("/", async (req, res, next) => {
  const person = await Persons.find({});
  res.status(200).json(person);
});

router.get("/:personId", async (req, res, next) => {
  const person = await Persons.findById(req.params.personId)
  res.status(200).json(person);

});
module.exports = router;
