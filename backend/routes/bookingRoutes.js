const express = require("express");
const router = express.Router();
const Booking = require("../models/booking");

router.post("/", async (req, res, next) => {
  const booking = new Booking(req.body);
  await booking.save();
  res.status(201).json(booking);
});

router.get("/", async (req, res, next) => {
  const booking = await Booking.find(req.params, 'selectedSeats');
  res.status(200).json(booking);
});

module.exports = router;