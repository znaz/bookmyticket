const mongoose = require("mongoose");

const moviesSchema = new mongoose.Schema({
  title: String,
  description: String,
  language: String,
  category: String,
  image: String
});

const Movies = mongoose.model("Movies", moviesSchema);

module.exports = Movies;
