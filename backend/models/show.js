const mongoose = require("mongoose");

const showSchema = new mongoose.Schema({
  movie : {
    type : mongoose.ObjectId,
    ref: 'Movies'
  },
  screen: {
    type : mongoose.ObjectId,
    ref : 'Screen'
  },
  showTime: Date
  
});

const Show = mongoose.model("Show", showSchema);

module.exports = Show;