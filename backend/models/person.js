const mongoose = require("mongoose");

const personsSchema = new mongoose.Schema({
  image: String,
  name: String,
  occupation: String,
  birthPlace: String,
  about: String,
});

const Persons = mongoose.model("Persons", personsSchema);

module.exports = Persons;
