const mongoose = require("mongoose");

const castSchema = new mongoose.Schema({
  person:{
    type: mongoose.ObjectId,
    ref: 'Persons'
  },
  movie:{
    type: mongoose.ObjectId,
    ref: 'Movies'
  },
  role: String
});

const Cast = mongoose.model("Cast", castSchema);

module.exports = Cast;